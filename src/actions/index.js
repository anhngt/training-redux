import * as type from '../constants/Type';
export const status = () => {
    return {
        type: type.STATUS
    }
};

export const sort = (sort) => {
    return {
        type: type.SORT,
        sort // sort : sort
    }
}