import * as types from "../../constants/Type";
export const listAll = () => {
  return {
    type: types.LIST_ALL,
  };
};

export const addProduct = (product) => {
  return {
    type: types.ADD_PRODUCT,
    product,
  };
};

export const findProduct = (id) => {
  return {
    type: types.EDIT_PROUCT,
    id: id,
  };
};

export const createData = (list) => {
  return {
    type: types.CREATE_PRODUCT,
    list: list,
  };
};

export const updateProduct = (inputs) => {
  return {
    type: types.UPDATE_PRODUCT,
    inputs: inputs,
  };
};

export const deleteProduct = (id) => {
  return {
    type: types.DELETE_PRODUCT,
    id: id,
  };
};

export const searchProduct = (inputs) => {
  return {
    type: types.SEARCH_PRODUCT,
    inputs: inputs,
  };
};
