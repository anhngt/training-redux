import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as types from "../../constants/Type";
import * as actions from "../../actions/products/index";

// import
class Form extends Component {
  // constructor(props) {
  //   super(props);

  //   // this.state = {
  //   //   name: "",
  //   //   status: "",
  //   // };
  // }

  randoms = () => {
    return Math.random().toString(36).substr(2, 15);
  };

  randomIds = () => {
    return this.randoms() + "-" + this.randoms() + "-" + this.randoms();
  };

  saveDataForm = (event) => {
    event.preventDefault();
    let name = event.target.name.value;
    let status = event.target.status.value;
    if (event.target.id && event.target.id.value) {
      let id = event.target.id.value;
      this.props.onUpdateProduct({
        id,
        name,
        status,
      });
      alert("Bạn đã thêm mới thành công!");
      this.props.history.push("/product/list");
    } else {
      let id = this.randomIds();
      this.props.onSaveProduct({
        name: name,
        status: status,
        id: id,
      });
      alert("Bạn đã thêm mới thành công!");
      this.props.history.push("/product/list");
    }
  };

  render() {
    let ids = this.props.match.params.id;
    let id = "";
    let name = "";
    let status = -1;
    if (ids) {
      let list = this.props.list;
      // console.log(id,this.props.onFindProduct(id))
      let product = list.find((item) => item.id === ids);
      if (product) {
        id = product.id;
        name = product.name;
        status = product.status;
      }
    }

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card-body">
                <h4 className="header-title mt-0">Thêm mới</h4>
                <p className="sub-header">Điền đầy đủ thông tin</p>
                <form className="form-horizontal" onSubmit={this.saveDataForm}>
                  <input type="hidden" name="id" defaultValue={id ? id : ""} />
                  <div className="row">
                    <div className="col">
                      <div className="form-group row">
                        <label
                          className="col-lg-2 col-form-label"
                          htmlFor="simpleinput"
                        >
                          Họ và tên
                        </label>
                        <div className="col-lg-10">
                          <input
                            type="text"
                            name="name"
                            className="form-control"
                            defaultValue={name ? name : ""}
                            placeholder="Họ và tên..."
                          />
                        </div>
                      </div>
                      <div className="form-group row">
                        <label
                          className="col-lg-2 col-form-label"
                          htmlFor="example-email"
                        >
                          Trạng thái
                        </label>
                        <div className="col-lg-10">
                          <select
                            defaultValue={status}
                            name="status"
                            className="form-control custom-select"
                          >
                            <option value={-1}>Chọn</option>
                            <option value={0}>Ẩn</option>
                            <option value={1}>Hiện thị</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      <button className="btn btn-primary">Thêm mới</button>
                      &nbsp;
                      <Link className="btn btn-danger" to="/product/list">
                        Hủy bỏ
                      </Link>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

let stateToProps = (state) => {
  return {
    list: state.products,
  };
};

let stateDischartProps = (dispatch, props) => {
  return {
    onSaveProduct: (product) => {
      dispatch(actions.addProduct(product));
    },
    onUpdateProduct: (inputs) => {
      dispatch(actions.updateProduct(inputs));
    },
  };
};
export default connect(stateToProps, stateDischartProps)(Form);
