import products from './products';
import { combineReducers } from 'redux';

const reducer = combineReducers({
    products
})
export default reducer;