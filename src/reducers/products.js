import * as types from "./../constants/Type";
import { findIndex, find } from "lodash";
let data = localStorage.getItem("list");
if (data) {
  data = JSON.parse(localStorage.getItem("list"));
}

let iniState = data ? data : [];

let removeAccents = (str) => {
  var AccentsMap = [
      "aàảãáạăằẳẵắặâầẩẫấậ",
      "AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ",
      "dđ", "DĐ",
      "eèẻẽéẹêềểễếệ",
      "EÈẺẼÉẸÊỀỂỄẾỆ",
      "iìỉĩíị",
      "IÌỈĨÍỊ",
      "oòỏõóọôồổỗốộơờởỡớợ",
      "OÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ",
      "uùủũúụưừửữứự",
      "UÙỦŨÚỤƯỪỬỮỨỰ",
      "yỳỷỹýỵ",
      "YỲỶỸÝỴ"
  ];
  for (var i = 0; i < AccentsMap.length; i++) {
      var re = new RegExp('[' + AccentsMap[i].substr(1) + ']', 'g');
      var char = AccentsMap[i][0];
      str = str.replace(re, char);
  }
  return str;
}

const reducer = (state = iniState, action) => {
  switch (action.type) {
    case types.LIST_ALL: {
      return state;
    }
    case types.ADD_PRODUCT: {
      state.push({
        id: action.product.id,
        name: action.product.name,
        status: action.product.status,
      });
      localStorage.setItem("list", JSON.stringify(state));
      return [...state];
    }
    case types.CREATE_PRODUCT: {
      state = action.list;
      localStorage.setItem("list", JSON.stringify(state));
      return [...state];
    }
    case types.UPDATE_PRODUCT: {
      let inputs = action.inputs;
      let key = findIndex(state, function (o) {
        return o.id == inputs.id;
      });
      state[key].name = inputs.name;
      state[key].status = inputs.status;
      localStorage.setItem("list", JSON.stringify(state));
      return [...state];
    }
    case types.DELETE_PRODUCT: {
      let id = action.id;
      state = state.filter((item) => item.id !== id);
      //  console.log(data);
      localStorage.setItem("list", JSON.stringify(state));
      return [...state];
    }
    case types.SEARCH_PRODUCT: {
      let inputs = action.inputs;
      let list = JSON.parse(localStorage.getItem("list"));
      
      state = list.filter((x) => {
        let temp = removeAccents(x.name);
        return temp.indexOf(inputs.name) != -1 && x.status == inputs.status;
    }) ;
      // state = list.filter(function (item) {
      //   if (inputs.name) {
      //     let status = item.name.indexOf(inputs.name);
      //     if (status === 0) {
      //       return item;
      //     }
      //   }
      //   // if (inputs.status === item.status) {
      //   //   return item;
      //   // }
      // });

      return [...state];
    }
    default: {
      return state;
    }
  }
};

export default reducer;
