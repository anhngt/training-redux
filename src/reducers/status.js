let installState = false;
let reducer = (state = installState, action) => {
    let data = state;
    switch (action.type) {
        case 'CHANGE_STATUS': {
            data = !state;
            break;
        }
        default: {
            //statements;
            break;
        }
    }

    return data;
}

export default reducer;