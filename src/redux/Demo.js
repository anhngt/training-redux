import { createStore } from 'redux';
import { status, sort } from '../actions/index';
import reducer from '../reducers/index';
// console.log(reducer);
const store = createStore(reducer);
store.dispatch(status());
store.dispatch(sort({
    by: 'name',
    value: -1
}));
console.log(store.getState());
console.log(store.getState());